#!/usr/bin/env bash
if [[ -z `ps ax|grep -a "bot/run"|grep -v grep |awk '{print $1}'` ]];
        then
        echo "$(date  +%Y-%m-%d\ %H:%M:%S) ERROR! Restart.";
        php /var/www/testbot.loc/yii bot/run &
        else echo "$(date  +%Y-%m-%d\ %H:%M:%S) OK";
fi
