<?php

namespace app\commands;

use app\models\Course;
use app\models\Tickers;
use yii;
use yii\console\Controller;
use yii\db\Exception;

class CourseController extends Controller
{
    public function actionUpdate()
    {
        $tickers = Tickers::getSupportedTickers();

        foreach ($tickers as $ticker)
        {
            $course = Course::askCourse($ticker);

            if (property_exists ($course,'price'))
            {
                try {
                    Course::saveCourse($ticker, $course->price);
                } catch (Exception $e) {
                }
            }
        }
        try {
            Course::deleteExpiriedCourses();
        } catch (Exception $e) {
            Yii::error($e->getMessage());
        }
    }
    public function actionTest()
    {
        var_dump(empty(null));
    }
}