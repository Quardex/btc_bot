<?php


namespace app\commands;

use app\models\BotRemote;
use app\models\Course;
use app\models\Subscription;
use app\models\Tickers;
use Askoldex\Teletant\Context;
use Askoldex\Teletant\Exception\TeletantException;
use Askoldex\Teletant\States\Scene;
use Askoldex\Teletant\States\Stage;
use yii;
use yii\console\Controller;

class SubController extends Controller
{
    public function actionConductor()
    {

        $subscriptions = Subscription::find()->all();

        /** @var Subscription $subscribe */
        foreach ($subscriptions as $subscribe) {
            $subscribe->preCheck();
            switch ($subscribe->type) {
                case Subscription::TYPE_SUBSCRIPTION_INTERVAL :
                    if ($subscribe->checkInterval()) {
                        $subscribe->sendMessage();
                    }
                    break;
                case Subscription::TYPE_SUBSCRIPTION_DYNAMIC :
                    $subscribe->collectInfo();
                    if ($subscribe->checkDiapason()) {
                        $subscribe->sendMessage();
                    }
                    break;

            }
        }
    }
}