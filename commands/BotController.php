<?php

namespace app\commands;

use app\models\BotRemote;
use app\models\Course;
use app\models\Message;
use app\models\Subscription;
use app\models\Tickers;
use Askoldex\Teletant\Exception\TeletantException;
use Askoldex\Teletant\States\Scene;
use Askoldex\Teletant\States\Stage;
use app\models\SceneStorage;
use Yii;
use yii\console\Controller;
use Askoldex\Teletant\Context;
use Askoldex\Teletant\Addons\Menux;


class BotController extends Controller
{
    public function actionRun()
    {
        $bot = new BotRemote();

        $stage = new Stage();
        $bot->onCommand('start', function (Context $ctx) {

            $ctx->enter('main');
        });

        /** Сцены */
        $mainScene = new Scene('main');
        $courseScene = new Scene('course');
        $noticeScene = new Scene('notice');
        $intervalScene = new Scene('interval');
        $dynamicScene = new Scene('dynamic');
        $chooseDynamicScene = new Scene('chooseDynamic');
        $chooseIntervalScene = new Scene('chooseInterval');
        $subscribeDynamicScene = new Scene('subscribeDynamic');
        $subControlScene = new Scene('subControl');

        /**   Главное меню      */
        $mainScene->onEnter(function (Context $ctx) {
            $mainMenu = Menux::Create('Главное меню')->inline();
            $mainMenu->row()->btn('🔔 Уведомления 🔔', 'notice');
            $mainMenu->row()->btn('📉 Курсы 📈', 'course');
            $mainMenu->row()->btn('📨 Управление подписками 📨', 'subControl');
            $ctx->callbackQuery()->isEmpty()
                ? $ctx->replyHTML('🏠  <code>Главное меню</code>', $mainMenu)
                : $ctx->editSelfHTML('🏠  <code>Главное меню</code>', $mainMenu);
        });
        /** События для кнопок */
        $mainScene->onAction('course', function (Context $ctx) {
            $ctx->enter('course');
        });
        $mainScene->onAction('notice', function (Context $ctx) {
            $ctx->enter('notice');
        });
        $mainScene->onAction('subControl', function (Context $ctx) {
            $ctx->enter('subControl');
        });

        /** Курсы */
        $tickers = Tickers::getSupportedTickers();
        $courseMenu = Menux::Create('Курсы')->inline();

        $bot->sortCourseMenu($tickers, $courseMenu);
        $courseMenu->row()->btn('◀️ Назад', 'back');

        $courseScene->onEnter(function (Context $ctx) use ($tickers, $courseMenu) {

            $ctx->editSelfHTML('📉  <code>Курсы</code>', $courseMenu);
        });
        $bot->sortCourseMenu($tickers, $courseMenu, $courseScene);
        $courseScene->onAction('back', function (Context $ctx) {
            $ctx->enter('main');
        });

        /** запись сцен */
        $stage->addScene($mainScene);
        $stage->addScene($courseScene);
        $stage->addScene($noticeScene);
        $stage->addScene($intervalScene);
        $stage->addScene($dynamicScene);
        $stage->addScene($chooseDynamicScene);
        $stage->addScene($chooseIntervalScene);
        $stage->addScene($subscribeDynamicScene);
        $stage->addScene($subControlScene);

        $bot->middlewares([
            function (Context $ctx, callable $next) {
                $storage = new SceneStorage();
                $storage->boot($ctx);
                $ctx->setStorage($storage);
                $next($ctx);
            },
            $stage->middleware(),

            Menux::Middleware()
        ]);

        $bot->setGlobalCommands([
            $mainScene,
            $courseScene,
            $noticeScene,
            $intervalScene,
            $dynamicScene,
            $chooseDynamicScene,
            $subscribeDynamicScene,
            $subControlScene,
            $chooseIntervalScene
        ]);

        /** Уведомления */
        $noticeMenu = Menux::Create('Уведомления')->inline();
        $noticeMenu->row()->btn('⏳  По интервалу  ⏳', 'interval');
        $noticeMenu->row()->btn('📊 По динамике курса 📊', 'dynamic');
        $noticeMenu->row()->btn('◀️ Назад', 'back');

        $noticeScene->onEnter(function (Context $ctx) use ($noticeMenu) {
            $ctx->editSelfHTML('🔔  <code>Уведомления</code>', $noticeMenu);
        });
        /** События для кнопок */
        $noticeScene->onAction('interval', function (Context $ctx) {
            $ctx->enter('interval');
        });
        $noticeScene->onAction('dynamic', function (Context $ctx) {
            $ctx->enter('dynamic');
        });
        $noticeScene->onAction('back', function (Context $ctx) {
            $ctx->enter('main');
        });


        $Subscription = new Subscription();
        $backMenu = Menux::Create('dynamic2')->inline()->btn('◀️ Назад', 'back');
        $msgForDel = new Message();

        /** Уведомления по интервалу */
        $intervalScene->onEnter(function (Context $ctx) use ($courseMenu, $Subscription) {
            $Subscription->type = Subscription::TYPE_SUBSCRIPTION_INTERVAL;
            $Subscription->user_id = $ctx->getUserID();
            $ctx->editSelfHTML('📈  <code>Выберите интересующий курс</code>', $courseMenu);
        });
        $intervalScene->onAction('back', function (Context $ctx) {
            $ctx->enter('notice');
        });
        $intervalScene->onAction("{ticker}", function (Context $ctx) use ($Subscription) {
            $Subscription->condition = 1;
            $ctx->enter('chooseInterval');
        });
        $intervalMenu = Menux::Create('interval')->inline();
        $intervalMenu->row()
            ->btn("+ 1 мин.", 'add.1')
            ->btn("- 1 мин.", 'add.-1')
            ->row()
            ->btn("+ 10 мин.", 'add.10')
            ->btn("- 10 мин.", 'add.-10')
            ->row()
            ->btn("+ 1 час.", 'add.60')
            ->btn("- 1 час.", 'add.-60')
            ->row()
            ->btn("+ 1 день.", 'add.1440')
            ->btn("- 1 день.", 'add.-1440')
            ->row()
            ->btn('✅ Установить интервал ⏳', 'done')
            ->row()
            ->btn('◀️ Назад', 'back');

        $chooseIntervalScene->onEnter(function (Context $ctx) use ($backMenu, $Subscription, $intervalMenu) {
            $ticker = $ctx->var('ticker');
            $Subscription->ticker_id = Tickers::getId($ticker);
            $Subscription->ticker = $ticker;
            $Subscription->condition += intval($ctx->var('interval'));

            $ctx->editSelfHTML("Интервал: " . $Subscription->time_elapsed(), $intervalMenu);
        });
        $chooseIntervalScene->onAction("add.{interval:integer}", function (Context $ctx) use ($backMenu, $Subscription, $intervalMenu) {

            if ($Subscription->condition + $ctx->var('interval') >= 2) {
                if ($Subscription->condition + $ctx->var('interval') <= $Subscription::MAX_INTERVAL) {
                    $Subscription->condition += $ctx->var('interval');
                } else {
                    $Subscription->condition = $Subscription::MAX_INTERVAL;
                    $text = "❕" . $Subscription->time_elapsed() . "  - максимальный интервал";
                }
            } else {
                $text = "❗️ Интервал не может быть меньше 1 минуты";
                $Subscription->condition = 1;
            }
            if (!isset($text)) {
                $text = '';
            }
            try {
                $ctx->editSelfHTML("Интервал: " . $Subscription->time_elapsed() . " \n\n $text", $intervalMenu);
            } catch (TeletantException $e) {
            }
        });
        $chooseIntervalScene->onAction('back', function (Context $ctx) {
            $ctx->enter('notice');
        });
        /** Уведомление о подписке */
        $chooseIntervalScene->onAction("done", function (Context $ctx) use ($Subscription) {
            $mainMenu = (Menux::Create('index')->inline())->row()->btn('🏠 В главное меню 🏠', 'home');
            $Subscription->subscribe();
            $ctx->editSelfHTML('✅ Вы успешно подписались на уведомления по валютной паре <code>' . $Subscription->ticker . '</code> с интервалом <code>' . $Subscription->time_elapsed(). '</code> В течении нескольких секунд мы пришлем вам первое уведомление, от него и начнется отсчет.', $mainMenu);
        });
        $chooseIntervalScene->onAction('home', function (Context $ctx) {
            $ctx->enter('main');
        });


        /** Уведомления по динамике */
        $dynamicScene->onEnter(function (Context $ctx) use ($courseMenu, $Subscription) {
            $Subscription->type = Subscription::TYPE_SUBSCRIPTION_DYNAMIC;
            $Subscription->user_id = $ctx->getUserID();
            $ctx->editSelfHTML('📈  <code>Выберите интересующий курс</code>', $courseMenu);
        });

        $dynamicScene->onAction('back', function (Context $ctx) {
            $ctx->enter('notice');
        });


        $dynamicScene->onAction("{ticker}", function (Context $ctx) {
            $ctx->enter('chooseDynamic');
        });

        $chooseDynamicScene->onEnter(function (Context $ctx) use ($backMenu, $Subscription, $msgForDel) {
            $ticker = $ctx->var('ticker');
            $Subscription->ticker_id = Tickers::getId($ticker);
            $Subscription->ticker = $ticker;
            $currency = explode('_', $ticker);
            $course = Course::getCourseByTicker($ticker);
            $example = Subscription::formatFloat($course * 0.015);
            $msgForDel->init($ctx);
            $ctx->editSelfHTML("📊 Введите отклонение от {$currency[0]} в валюте {$currency[1]} при котором необходимо выслать уведомление. \nКурс выбранной пары: <code>{$course}</code>\nПример: <code>{$example}</code> (разделитель точка)", $backMenu);
        });
        $chooseDynamicScene->onAction('back', function (Context $ctx) {
            $ctx->enter('dynamic');
        });

        /** Прием динамики и валидация */
        $chooseDynamicScene->onText('{dynamic:float}', function (Context $ctx) use ($Subscription, $backMenu, $bot, $msgForDel) {
            $msgForDel->delete($ctx, $bot);
            $dynamic = round($ctx->var('dynamic'), 8);
            if ($dynamic == 0) {
                $msgForDel->delete($ctx, $bot);
                $ctx->reply('❗️Введите число больше 0', $backMenu);
            } else {
                $dynamic < 0 ? $dynamic = abs($dynamic) : '';
                $Subscription->condition = $dynamic;
                $ctx->enter('subscribeDynamic');
            }
        }, function (Context $ctx, $errors) use ($backMenu, $bot, $msgForDel) {
            switch ($errors['dynamic']) {
                case 'invalid_type':
                    $msgForDel->delete($ctx, $bot);
                    $ctx->reply('❗️Введите число', $backMenu);
                    break;
                case 'not_specified':
                    $msgForDel->delete($ctx, $bot);
                    $ctx->reply('❗️Поле dynamic обязательно для заполнения', $backMenu);
                    break;
            }
        });

        /** Уведомление о подписке */

        $subscribeDynamicScene->onEnter(function (Context $ctx) use ($Subscription) {
            $mainMenu = (Menux::Create('index')->inline())->row()->btn('🏠 В главное меню 🏠', 'back');
            $dynamic = $Subscription::formatFloat($Subscription->condition);
            $ticker = $Subscription->ticker;
            $Subscription->subscribe();
            $ctx
                ->with('dynamic', $dynamic)
                ->with('ticker', $ticker)
                ->replyHTML("✅ Вы успешно подписались на получение уведомлений по валютной паре <code>{$ticker}</code>\n\nВы будете получать уведомления при изменении на указанное вами значение: <code>{dynamic}</code> , относительно последнего отчёта.\n\nБуквально через несколько секунд мы сгенерируем первый отчет, в котором будет курс выбранной пары, от него и начнется отсчет.", $mainMenu);
        });
        $subscribeDynamicScene->onAction('back', function (Context $ctx) {
            $ctx->enter('main');
        });

        $subControlScene->onEnter(function (Context $ctx) {
            $Subs = Subscription::findAll(['user_id' => $ctx->getUserID()]);
            $subMenu = Menux::Create('Управление подписками')->inline();
            foreach ($Subs as $sub) {
                $sub->collectInfo();
                $btnText = $sub->textGenerate();
                $subMenu->row()->btn($btnText, 'checkDelete.' . $sub->id);
            }
            if (!empty($Subs)) {
                $subMenu->row()->btn('🗑 Удалить все подписки 🗑', 'delAll');
            }
            $subMenu->row()->btn('📨 Подписаться на уведомления 📨', 'notice');

            $subMenu->row()->btn('◀️ Назад', 'back');
            if (empty($Subs)) {
                $ctx->editSelfHTML("⚙️  <code>Управление подписками</code>️\n\nУ вас нет активных подписок. Для того чтобы получать уведомления по интересующей вас валютной паре -  задайте параметры, нажав кнопку - \"<code>Подписаться на уведомления</code>\"", $subMenu);
            } else {
                $ctx->editSelfHTML("⚙️  <code>Управление подписками</code>️\n\nДля того чтобы отписаться от уведомлений - выберите ненужную подписку либо нажмите кнопку - \"<code>Удалить все подписки</code>\"", $subMenu);
            }
        });
        $subControlScene->onAction('checkDelete.{id}', function (Context $ctx) {
            $checkDeleteMenu = Menux::Create('checkDel')->inline();
            $checkDeleteMenu->row()->btn('☑️ Да', 'delete.' . $ctx->var('id'));
            $checkDeleteMenu->row()->btn('❌️ Нет', 'subControl');
            $ctx->editSelfHTML('🤔 Вы уверены что хотите удалить выбранную подписку?', $checkDeleteMenu);
        });
        $subControlScene->onAction('delete.{id}', function (Context $ctx) {
            $sub = Subscription::findOne(['id' => $ctx->var('id')]);
            $sub->delete();
            $ctx->enter('subControl');
        });
        $subControlScene->onAction('notice', function (Context $ctx) {
            $ctx->enter('notice');
        });
        $subControlScene->onAction('delAll', function (Context $ctx) {
            $checkDeleteMenu = Menux::Create('checkDel')->inline();
            $checkDeleteMenu->row()->btn('☑️ Да', 'allDelConfirm');
            $checkDeleteMenu->row()->btn('❌️ Нет', 'subControl');
            $ctx->editSelfHTML('🤔 Вы уверены что хотите отписаться от всех уведомлений?', $checkDeleteMenu);
        });
        $subControlScene->onAction('allDelConfirm',function (Context $ctx) {
            $mainMenu = (Menux::Create('index')->inline())->row()->btn('🏠 В главное меню 🏠', 'back');
            $Subs = Subscription::findAll(['user_id' => $ctx->getUserID()]);
            foreach ($Subs as $sub) {
                $sub->delete();
            }
            $ctx->editSelfHTML("Вы успешно отписались от всех уведомлений", $mainMenu);
        });
        $subControlScene->onAction('subControl', function (Context $ctx) {
            $ctx->enter('subControl');
        });
        $subControlScene->onAction('back', function (Context $ctx) {
            $ctx->enter('main');
        });

        try {
            $bot->polling();
        } catch (TeletantException $e) {
            Yii::warning($e->getMessage());
        }
    }
}
