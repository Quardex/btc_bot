<?php

namespace app\commands;

use app\models\Course;
use app\models\Tickers;
use yii;
use yii\console\Controller;
use yii\db\Exception;

class TickersController extends Controller
{
    public function actionUpdate()
    {
        $tickers = Tickers::getTickers();
        $supportedTickers = [];
        foreach ($tickers as $ticker)
        {
            $ask = Course::askCourse($ticker);
            if ($ask->price)
            {
                array_push($supportedTickers,$ticker);
            }
        }
        foreach ($supportedTickers as $supportedTicker) {
            try {
                Tickers::updateTicker($supportedTicker);
            } catch (Exception $e) {
                Yii::error($e->getMessage());
            }
        }

    }

    public function actionTest()
    {
    }
}