<?php

use app\models\Currency;
use yii\db\Migration;

/**
 * Class m200409_082042_insert_base_currencies
 */
class m200409_082042_insert_base_currencies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Currency::tableName(),[
            'name' => 'BTC',
            'enable' => true
        ]);

        $this->insert(Currency::tableName(),[
            'name' => 'USDT',
            'enable' => true
        ]);

        $this->insert(Currency::tableName(),[
            'name' => 'ETH',
            'enable' => true
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200409_082042_insert_base_currencies cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200409_082042_insert_default_currencies cannot be reverted.\n";

        return false;
    }
    */
}
