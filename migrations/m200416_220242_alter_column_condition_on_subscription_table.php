<?php

use yii\db\Migration;
use app\models\Subscription;
/**
 * Class m200416_220242_alter_column_condition_on_subscription_table
 */
class m200416_220242_alter_column_condition_on_subscription_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(Subscription::tableName(), 'condition',$this->float(8));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200416_220242_alter_column_condition_on_subscription_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_220242_alter_column_comdition_on_subscription_table cannot be reverted.\n";

        return false;
    }
    */
}
