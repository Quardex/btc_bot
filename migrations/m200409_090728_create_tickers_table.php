<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tickers}}`.
 */
class m200409_090728_create_tickers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tickers}}', [
            'id' => $this->primaryKey(),
            'ticker' => $this->string(16)->unique()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tickers}}');
    }
}
