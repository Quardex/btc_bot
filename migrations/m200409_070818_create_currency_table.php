<?php

use app\models\Currency;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%currency}}`.
 */
class m200409_070818_create_currency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Currency::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string(8)->unique(),
            'enable' => $this->boolean()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
