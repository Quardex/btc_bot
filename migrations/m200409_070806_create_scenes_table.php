<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%scenes}}`.
 */
class m200409_070806_create_scenes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%scenes}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11)->unique(),
            'scene' => $this->string(50)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%scenes}}');
    }
}
