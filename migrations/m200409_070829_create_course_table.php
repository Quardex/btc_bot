<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course}}`.
 */
class m200409_070829_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(),
            'timestamp' => $this->timestamp(),
            'ticker' => $this->string(16),
            'price' => $this->float()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%course}}');
    }
}
