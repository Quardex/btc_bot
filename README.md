# Telegram BTC BOT

Бот для получения курсов криптовалют. 

### Требования

* PHP версия 7.2 и выше

### Установка

Проект закачан, создаем базу данных и указываеб её параметры в `/config/db.php` .
В конфиге `/config/param.php` устанавливаем токен вашего бота и при необходимости *proxy* 

*Для работы proxy необходимо раскоментировать строку в* `/models/BotRemote:setSettings()`.

```
composer install 
php yii migrate
php yii tickers/update
```
Для сбора курсов добавляем в крон скрипт.

Не забудьте в примерах заменить название папки проекта на своё.

**Для Windows OpenServer:**
```
*/1 * * * *
%progdir%\modules\php\%phpdriver%\php-win.exe -c %progdir%\modules\php\%phpdriver%\php.ini -q -f %sitedir%\Папка проекта\yii course/update
```

**Для Linux :**
```
*/1 * * * * * /usr/bin/php /var/www/Папка проекта/yii course/update >> /var/www/Папка проекта/runtime/log/crontab.log
```

Так-же необходимо выполнять в кроне скрипт проверки подписок и отправки уведомлений

**Для Windows OpenServer:**
```
*/1 * * * *
%progdir%\modules\php\%phpdriver%\php-win.exe -c %progdir%\modules\php\%phpdriver%\php.ini -q -f %sitedir%\Папка проекта\yii sub/conductor
```

**Для Linux :**
```
*/1 * * * * * /usr/bin/php /var/www/Папка проекта/yii sub/conductor >> /var/www/Папка проекта/runtime/log/crontab.log
```
Так-же для Linux добавьте в крон скрипт поддержания работы бота:

```
*/1 * * * * * /var/www/Папка проекта/bot-run.sh >> /var/www/Папка проекта/runtime/log/crontab.log
```
Для Windows поддержку работы скрипта не завез 🤷‍

по этому ручками запускаем из папки проекта в консоли `php yii bot/run` и радуемся.

Так же рекомендую проверить значения в конфигах:

#### mysql / mariaDB ####

```
interactive_timeout             = 28800
wait_timeout                    = 28800
max_allowed_packet              = 128M (меньше не рекомендую)
```

#### PHP ####
```
extension                    = pdo_mysql
error_reporting              = E_ALL & ~E_NOTICE ~E_WARNING
```
Последнее что делаем для Linux, добавляем в крон скрипт перезапуска бота раз в 6 часов, чтобы освежить коннекты:

```
* */6 * * * * /var/www/Папка проекта/bot-restart.sh >> /var/www/Папка проекта/runtime/log/crontab.log
```
Для windows костылите сами.

### Ссылки ###
* [Crypto Notifier Bot](http://t.me/cryptonot_bot) - Релиз бота
* [Viktor Budko](http://t.me/talk_to_vbudko_bot) - Написать автору бота



