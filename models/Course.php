<?php


namespace app\models;

use yii;
use yii\db\ActiveRecord;

class Course extends ActiveRecord
{
    const STORAGE_PERIOD = '7 day';
    const DEVIATION_RANGE = '30 seconds';
    const URL = "https://api.binance.com/api/v3/ticker/price?symbol=";

    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * Получить последние курсы.
     *
     * Примеры:
     *      1) getCourse('BTC', 'USD') =
     *          8446.56
     *
     *      2) getCourse('BTC', ['USD']) = [
     *          'USD' => 8446.56
     *      ]
     *
     *      3) getCourse('BTC', ['USD, 'EUR']) = [
     *          'USD' => 8446.56,
     *          'EUR' => 6419.3
     *      ]
     *
     *      4) getCourse(['BTC'], 'USD') = [
     *          'BTC' => 8446.56
     *      ]
     *
     *      5) getCourse(['BTC', 'ETH'], 'EUR') = [
     *          'BTC' => 6419.3,
     *          'ETH' => 391.224
     *      ]
     *
     * @param array|string $from
     * @param array|string $to
     * @param string $timestamp
     * @return mixed
     */
    public static function getCourse($from, $to, $timestamp = null)
    {
        $result = null;

        if (is_array($from)) {
            foreach ($from as $cur) {
                $direct[$cur] = "{$cur}_{$to}";
                $reverse[$cur] = "{$to}_{$cur}";
                if ($cur == $to) {
                    $result[$cur] = 1;
                }
            }
        } elseif (is_array($to)) {
            foreach ($to as $cur) {
                $direct[$cur] = "{$from}_{$cur}";
                $reverse[$cur] = "{$cur}_{$from}";
                if ($cur == $from) {
                    $result[$cur] = 1;
                }
            }
        } else {
            if ($from == $to) {
                $result = 1;
            } else {
                $direct[] = "{$from}_{$to}";
                $reverse[] = "{$to}_{$from}";
            }
        }

        if (isset($direct, $reverse)) {
            $in = array_merge(array_values($direct), array_values($reverse));
            $query = (new \yii\db\Query())
                ->select('max(timestamp)')
                ->from(self::tableName())
                ->where(['in', 'ticker', $in]);
                if (isset($timestamp))
                {
                    $query->andWhere(['=','timestamp', $timestamp]);
                }
                $query->groupBy('ticker');

            $courses = self::find()
                ->select(['ticker', 'price'])
                ->where(['in', 'ticker', $in])
                ->andWhere(['in', 'timestamp', $query])
                ->asArray()
                ->all();
        }

        if (!empty($courses)) {
            foreach ($courses as $course) {
                if (($cur = array_search($course['ticker'], $direct)) !== false) {
                    if ($cur) {
                        $result[$cur] = $course['price'];
                    } else {
                        $result = $course['price'];
                    }
                } elseif (($cur = array_search($course['ticker'], $reverse)) !== false) {
                    if ($cur) {
                        $result[$cur] = 1 / $course['price'];
                    } else {
                        $result = 1 / $course['price'];
                    }
                }
            }
        }

        return $result;
    }

    public static function getCourseByTicker($ticker, $timestamp = null)
    {
        $currencies = explode("_", $ticker);

        return self::getCourse($currencies[0], $currencies[1],$timestamp);
    }

    public static function askCourse($ticker)
    {
        $currencies = explode("_", $ticker);
        $ticker = $currencies[0] . $currencies[1];
        $url = self::URL . $ticker;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)");
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        return $result;
    }

    /**
     * @param $ticker string
     * @param $price float
     * @throws yii\db\Exception
     */
    public static function saveCourse($ticker, $price)
    {
        Yii::$app->db->createCommand()->insert(self::tableName(),
            [
                'timestamp' => date(Yii::$app->params['dateFormat']),
                'ticker' => $ticker,
                'price' => $price
            ])->execute();
    }

    public static function deleteExpiriedCourses()
    {
        self::deleteAll( [ '<=', 'timestamp', date( Yii::$app->params['dateFormat'], strtotime( '-' . self::STORAGE_PERIOD ) ) ] );
    }
}