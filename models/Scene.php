<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Scene
 * @package app\models
 * @property $id
 * @property $user_id
 * @property $scene
 */
class Scene extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%scenes}}';
    }

    public function setScene($sceneName)
    {
        Yii::$app->db->createCommand()->upsert('scenes',
            [
                'user_id' => $this->user_id,
                'scene' => $sceneName
            ],
            ['scene' => $sceneName])->execute();
    }
}