<?php


namespace app\models;


use Askoldex\Teletant\Addons\Menux;
use Askoldex\Teletant\Bot;
use Askoldex\Teletant\Context;
use Askoldex\Teletant\Settings;
use Askoldex\Teletant\States\Scene;
use yii;

class BotRemote extends Bot
{
    /**
     * BotRemote constructor.
     */
    public function __construct()
    {
        $settings = $this->setSettings();
        parent::__construct($settings);
    }

    /**
     * @return Settings
     */
    private function setSettings()
    {
        $settings = new Settings(Yii::$app->params['tokenBot']);
//        $settings->setProxy(Yii::$app->params['proxy']);
        $settings->setHookOnFirstRequest(false);

        return $settings;
    }

    public function setGlobalCommands(array $scenes)
    {
        foreach ($scenes as $scene) {
            $scene->onCommand('exit', function (Context $ctx) {
                $ctx->leave();
            });
            $scene->onCommand('start', function (Context $ctx) {
                $ctx->enter('main');
            });
        }
    }

    /**
     * @param array $array
     * @param Menux|null $menu
     * @param Scene|null $scene
     */
    public function sortCourseMenu(array $array, Menux $menu = null, Scene $scene = null)
    {
        $i = 0;
        foreach ($array as $item) {
            $i++;

            if ($menu && !$scene) {
                if ($i % 2 != 0) {
                    $menu->row();
                }

                $menu->btn($item, $item);
            }
            if ($scene && $menu) {
                $scene->onAction($item, function (Context $ctx) use ($item, $menu) {
                    $course = Course::getCourseByTicker($item);
                    $text = "<code>Курсы</code> \n" . $item . " курс = <code>" . $course . "</code>";
                    if (strcmp(strip_tags($text),strip_tags($ctx->callbackQuery()->message()->text()))) {
                        $ctx->editSelfHTML($text, $menu);
                    }
                });
            }
        }
    }
}