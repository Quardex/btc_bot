<?php


namespace app\models;

use yii;
use yii\db\ActiveRecord;

class Tickers extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%tickers}}';
    }

    public static function getId($ticker)
    {
        $data = self::findOne(['ticker' => $ticker]);
        return $data['id'];
    }
    public static function getTickers()
    {
        $currencies = Currency::getAll(true);
        $tickers = [];
        foreach ($currencies as $from)
        {
            foreach ($currencies as $to) {

                if ($from != $to)
                {
                    $tickers[] = $from."_".$to;
                }
            }
        }
        return $tickers;
    }

    /**
     * @param $ticker string
     * @throws yii\db\Exception
     */
    public static function updateTicker($ticker)
    {

            Yii::$app->db->createCommand()->upsert(self::tableName(),
                ['ticker' => $ticker],
                ['ticker' => $ticker])->execute();
    }

    public static function getSupportedTickers()
    {
        return array_column(self::find()->all(),'ticker');
    }
}