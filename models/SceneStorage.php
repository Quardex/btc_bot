<?php


namespace app\models;

use Askoldex\Teletant\Context;
use Askoldex\Teletant\Interfaces\StorageInterface;
use yii;
use yii\db\ActiveRecord;
use yii\db\Query;


class SceneStorage implements StorageInterface
{
    private $storage;
    /**
     * @var Context
     */
    private $ctx;

    public function boot(Context $ctx)
    {
        $this->ctx = $ctx;
    }

    public function setScene(string $sceneName)
    {
        Yii::$app->db->createCommand()->upsert('scenes',
            [
                'user_id' => $this->ctx->getUserID(),
                'scene' => $sceneName
            ],
            ['scene' => $sceneName])->execute();
    }

    public function getScene(): string
    {
        $scene = Scene::findOne(['user_id' => $this->ctx->getUserID()]);
        if ($scene instanceof Scene){
            if ($scene->scene == null)
            {
                return '';
            } else {
                return $scene->scene;
            }
        } return '';
    }

    public function setTtl(string $sceneName, int $seconds)
    {
        $this->storage[$this->ctx->getUserID()]['scene']['ttl'] = $seconds;
    }

    public function getTtl(string $sceneName)
    {
        return $this->storage[$this->ctx->getUserID()]['scene']['ttl'];
    }
}