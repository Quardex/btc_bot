<?php


namespace app\models;

use yii\db\ActiveRecord;

class Currency extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%currency}}';
    }
    public static function getAll($allowedOnly = false)
    {
        $query = self::find();

        if( $allowedOnly) {
            $query->andWhere( [ 'enable' => true ] );
        }

        return array_column($query->all(),'name');
    }
}