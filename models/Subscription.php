<?php


namespace app\models;

use Askoldex\Teletant\Exception\TeletantException;
use yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class Subscription
 * @package app\models
 * @property $id;
 * @property $user_id;
 * @property $type;
 * @property $condition;
 * @property $ticker_id;
 * @property $data;
 */
class Subscription extends ActiveRecord
{
    const TYPE_SUBSCRIPTION_INTERVAL = 1;
    const TYPE_SUBSCRIPTION_DYNAMIC = 2;

    const MAX_INTERVAL = 5 * 24 * 60; // 5 дней

    public $ticker;
    public $course;
    public $lastCourse;
    public $lastResponse;

    public $profit;
    public $currency;
    public $profitOnCurrency;
    public $typeName;

    public static function tableName()
    {
        return '{{%subscriptions}}';
    }

    public function subscribe()
    {
        $query = (new Query())->select('id')
            ->where(['user_id' => $this->user_id, 'type' => $this->type, 'ticker_id' => $this->ticker_id])->from(self::tableName())
            ->orderBy(['id' => SORT_DESC])->one();

        if (isset($query['id'])) {
            $this->updateLastResponse($query['id']);
        } else {
            Yii::$app->db->createCommand()->insert(self::tableName(),
                [
                    'user_id' => $this->user_id,
                    'type' => $this->type,
                    'condition' => $this->condition,
                    'ticker_id' => $this->ticker_id,
                    'data' => json_encode(['last_response' => date(Yii::$app->params['dateFormat']), 'last_course' => $this->course])
                ])->execute();
        }

    }

    public function preCheck()
    {
        $data = json_decode($this->data);
        $this->lastCourse = $data->last_course;
        $this->lastResponse = $data->last_response;
    }

    public function checkInterval()
    {
        if (!$this->lastCourse)
        {
            return true;
        }
        return $this->condition <= round(abs(time() - strtotime($this->lastResponse)) / 60, 0,PHP_ROUND_HALF_UP) ;
    }

    public function checkDiapason()
    {
        if (empty($this->lastCourse)) {
            return true;
        } else {
            $values = [$this->lastCourse, $this->course];
            if (max($values) - min($values) >= $this->condition) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function collectInfo()
    {
        $this->getTicker();
        $this->getCourse();
        $this->getType();
        $this->getCurrency();
    }

    public function profitOrganizer()
    {
        if (!empty($this->lastCourse)) {
            $this->profit = -round(($this->lastCourse - $this->course) / $this->lastCourse * 100, 2);
            $this->profitOnCurrency = ($this->lastCourse - $this->course) * -1;
        }
    }

    public function updateLastResponse($id = null, $date = null)
    {
        $data = json_encode(['last_response' => $date, 'last_course' => $this->course]);
        if (isset($this->id)) {
            $id = $this->id;
        }
        Yii::$app->db->createCommand()->update(self::tableName(), ['condition' => $this->condition, 'data' => $data], ['id' => $id])->execute();
    }

    /**
     * @param $type
     * @return string
     */
    public function generateText($type)
    {
        if (!$this->course) {
            $this->collectInfo();
        }
        $this->profitOrganizer();
        if ($this->profit > 0) {
            $indicator = '🍏';
            $icon = '🔺';
            $symbol = '+';
        } else {
            $indicator = '🍎';
            $icon = '🔻';
            $symbol = '';
        }
        switch ($type) {
            case self::TYPE_SUBSCRIPTION_INTERVAL :
                if ($this->profitOnCurrency) {
                    $this->profitOnCurrency = $this->formatFloat($this->profitOnCurrency);
                    $text = "$indicator ⏳ $this->ticker - <code>$this->course</code> \n\n<code>$symbol"."$this->profitOnCurrency</code> $this->currency $icon <code>$this->profit"."%</code>";
                } elseif (empty($this->lastCourse)) {
                    $text = "$this->ticker - <code>$this->course</code>";
                } else {
                    $text = null;
                }
                break;
            case self::TYPE_SUBSCRIPTION_DYNAMIC :
                if ($this->profitOnCurrency) {
                    $this->profitOnCurrency = $this->formatFloat($this->profitOnCurrency);
                    $text = "$indicator 📊 $this->ticker : <code>$this->course</code> \n\n <code>$symbol"."$this->profitOnCurrency</code> $this->currency $icon <code>$symbol". $this->profit ."%</code> \n\n";
                } elseif (empty($this->lastCourse)) {
                    $text = "$this->ticker - <code>$this->course</code>";
                } else {
                    $text = "За 7 дней валютная пара не изменялась более чем на $this->condition $this->currency";
                }
                break;
            default :
                $text = null;
                break;
        }
        return $text;
    }

    public function getType()
    {
        switch ($this->type) {
            case self::TYPE_SUBSCRIPTION_INTERVAL :
                $this->typeName = 'по интервалу ⏱ ';
                break;
            case self::TYPE_SUBSCRIPTION_DYNAMIC :
                $this->typeName = 'по динамике 📈 ';
                break;
            default :
                $this->typeName = '';
        }
    }

    public function getTicker()
    {
        $this->ticker = Tickers::findOne(['id' => $this->ticker_id])['ticker'];
    }

    public function getCourse()
    {
        $this->course = Course::getCourseByTicker($this->ticker);
    }

    public function getCurrency()
    {
        $this->currency = explode('_', $this->ticker)[1];
    }

    public function textGenerate()
    {
        $text = "🔘 $this->ticker $this->typeName ";
        switch ($this->type) {
            case self::TYPE_SUBSCRIPTION_INTERVAL :
                $text .= $this->time_elapsed();
                break;
            case self::TYPE_SUBSCRIPTION_DYNAMIC :
                $text .= self::formatFloat($this->condition)." $this->currency";
                break;
        }
        return $text;
    }

    public function sendMessage()
    {
        $text = $this->generateText($this->type);
        if ($text) {
            try {
                $bot = new BotRemote();
                $bot->Api()->sendMessage(['chat_id' => $this->user_id, 'text' => $text, 'parse_mode' => 'html']);
            } catch (TeletantException $e) {
                if ($e->getCode() == 403) {
                    $scene = Scene::findOne(['user_id' => $this->user_id]);
                    $scene->setScene('');
                    $this->delete();
                } else {
                    Yii::warning($e->getMessage());
                }
            }
            $this->updateLastResponse(null, date(Yii::$app->params['dateFormat']));
        }
    }

    function time_elapsed()
    {
        $min = $this->condition;
        $bit = array(
            ' д.' => $min / 1440 % 7,
            ' ч.' => $min / 60 % 24,
            ' мин.' => $min % 60,
        );

        foreach ($bit as $k => $v)
            if ($v > 0) $ret[] = $v . $k;

        return join(' ', $ret);
    }

    public static function formatFloat($float)
    {
       return rtrim(rtrim(Yii::$app->formatter->asDecimal(round($float, 8),8),'0'),'.');
    }
}