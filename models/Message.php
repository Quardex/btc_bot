<?php
namespace app\models;
use app\models\BotRemote;
use Askoldex\Teletant\Context;
use Askoldex\Teletant\Exception\TeletantException;

class Message
{
    public $message_id;
    public $chat_id;

    public function init(Context $ctx)
    {
        $this->message_id = $ctx->getMessageID();
        $this->chat_id = $ctx->getChatID();
    }
    public function delete(Context $ctx, BotRemote $bot)
    {
        try {
            $bot->Api()->deleteMessage((array)$this);
        } catch (TeletantException $e) {
        }
        $this->message_id = $ctx->getMessageID() + 1;
        $this->chat_id = $ctx->getChatID();
    }
}