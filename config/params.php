<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'tokenBot' => '1097576874:AAFlRuMEz8vx-uBH8WsgnQ9ehbb5Fa2_Tc4',
    'proxy' => 'socks5://96.96.33.133:1080',
    'dateFormat' => 'Y-m-d H:i:s'
];
